section .text

extern string_equals
global find_word

;rdi-указатель на нуль-терминированную строку
;rsi-указатель на начало словаря
find_word:
	push rdi
	push rsi
	.loop:
	add rsi, 8
	push rdi
	push rsi
	call string_equals
	pop rsi
	pop rdi
	sub rsi, 8
	test rax, rax
	jnz .end
	mov rsi, [rsi]
	test rsi, rsi
	jnz .loop

	.end:
	mov rax, rsi
	pop rsi
	pop rdi
	ret