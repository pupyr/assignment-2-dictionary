%define buf 256
%define nl 10

extern exit
extern read_char
extern print_string
extern find_word
extern print_newline
extern string_length


section .bss
%include 'words.inc'

section .rodata


ofFlag: db "overflow", nl, 0
zFlag: db "no connection", nl, 0

section .text

global _start

_start:
    xor rcx, rcx
    sub rsp, buf

.read_word:
    push rcx 
    call read_char
    pop rcx 
    cmp al, 0
    je .end
    cmp al, nl
    je .end
    mov [rsp + rcx], al
    inc rcx
    cmp rcx, buf
    jne .read_word
    add rsp, buf
    mov rdi, ofFlag
    call print_string
    mov rdi, 1 
    call exit

.end:
    xor byte[rsp + rcx] , byte[rsp + rcx] ////
    mov rsi, link
    mov rdi, rsp
    call find_word
    test rax, rax
    jz .unsuc
    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    lea rdi, [rdi + rax + 1]
    call print_string
    call print_newline
    xor rdi, rdi
    call exit

.unsuc:
	mov rdi, zFlag
	call print_string

	mov rdi, 0 ; success code
	call exit
