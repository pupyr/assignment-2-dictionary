
oFlag=-o
gFlag=-g
comLd=ld
comNsm=nasm
comRm=rm

all:hello

hello: lib.o main.o dict.o
	$(comLd) $(oFlag) main main.o dict.o lib.o

lib.o:
	$(comNsm) $(gFlag) -felf64 $(oFlag) lib.o lib.asm

main.o:
	$(comNsm) $(gFlag) -felf64 $(oFlag) main.o main.asm

dict.o:
	$(comNsm) $(gFlag) -felf64 $(oFlag) dict.o dict.asm

clean:
	$(comRm) *.o
	$(comRm) main
