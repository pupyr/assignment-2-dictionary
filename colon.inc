%define link 0

%macro colon 2
	%%this:
	dq link
	db %1, 0
	%2:

	%define link %%this
%endmacro